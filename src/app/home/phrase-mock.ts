import { Phrase } from '../shared/phrase.model';

export const PHRASES: Phrase[] = [
    { phraseEng: '' ,
    phrasePtBr: 'newton',
    image:  ['../../assets/ne.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: 'mapa de karnaugh',
    image: ['../../assets/kar.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: 'teorema de thevenin',
    image: ['../../assets/the.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: 'equacao booleana',
    image: ['../../assets/equacaobooleana.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: 'edo',
    image: ['../../assets/ed.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: 'tabela verdade',
    image: ['../../assets/tv.jpg'] },

    { phraseEng: '' ,
    phrasePtBr: '',
    image: ['../../assets/win.jpg'] }
];
