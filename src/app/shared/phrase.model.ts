export class Phrase {
  constructor(public phraseEng: string, public phrasePtBr: string, public image: Object[] = []) { }
}
