import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Authentication } from '../services/authentication.service';

import { Phrase } from '../shared/phrase.model';
import { PHRASES } from '../home/phrase-mock';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public get authentication(): Authentication {
    return this._authentication;
  }
  public set authentication(value: Authentication) {
    this._authentication = value;
  }

  @ViewChild('publicacoes') public publicacoes: any;

  public phrases: Phrase[] = PHRASES;
  public instruction = 'Solve Enigma:';
  public answer = '';

  public round = 0;
  public roundPhrase: Phrase;

  public progress = 0;

  public attempts = 3;

  public jogoEmAndamento = true;
  public tipoEncerramento: string;

  @Output() public endGame: EventEmitter<string> = new EventEmitter();

  constructor(
    private _authentication: Authentication
  ) {
    this.updateRound();
  }

  ngOnInit() {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
  }

  public updateResponse(answer: Event): void {
    this.answer = (<HTMLInputElement>answer.target).value;
  }

  public verifyAwnser(): void {
    if (this.roundPhrase.phrasePtBr === this.answer) {

      this.round++;

      this.progress = this.progress + (100 / this.phrases.length);

      if (this.round === 7) {
        this.endGame.emit('winner');
      }

      this.updateRound();

    } else {
      this.attempts--;

      if (this.attempts === -1) {
        this.endGame.emit('lose');
      }
    }
  }

  public updateRound(): void {

    this.roundPhrase = this.phrases[this.round];

    this.answer = '';
  }

  public encerrarJogo(tipo: string): void {
    this.jogoEmAndamento = false;
    this.tipoEncerramento = tipo;
  }

  public reiniciarJogo(): void {
  this.jogoEmAndamento = true;
  this.tipoEncerramento = undefined;
  }

}

