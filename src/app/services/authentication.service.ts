import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../acess/usuario.model';
import * as firebase from 'firebase';

@Injectable()
export class Authentication {

    public token_id: string;

    constructor(private router: Router) { }

    public registerUser(user: User): Promise<any> {

        return firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
            .then((anwser: any) => {

                delete user.password;

                firebase.database().ref(`user_details/${btoa(user.email)}`)
                    .set( user );

            })
            .catch((error: Error) => {
                console.log(error);
            });
    }

    public authenticate(email: string, password: string): void {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((anwser: any) => {
                firebase.auth().currentUser.getIdToken()
                    .then((idToken: string) => {
                        this.token_id = idToken;
                        localStorage.setItem('idToken', idToken);
                        this.router.navigate(['/home']);
                    });
            })
            .catch((error: Error) => console.log(error));
    }

    public authenticated(): boolean {

        if (this.token_id === undefined && localStorage.getItem('idToken') != null) {
            this.token_id = localStorage.getItem('idToken');
        }

        if ( this.token_id === undefined ) {
            this.router.navigate(['/']);
        }

        return this.token_id !== undefined;
    }

    public getOut(): void {

        firebase.auth().signOut()
            .then(() => {
                localStorage.removeItem('idToken');
                this.token_id = undefined;
                this.router.navigate(['/']);
            });
    }
}
