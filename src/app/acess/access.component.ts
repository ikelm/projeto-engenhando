import { Component, OnInit } from '@angular/core';
// import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss'],
})

export class AcessComponent implements OnInit {

  public register = false;

  constructor() { }

  ngOnInit() {
  }

  public displayPanel(event: string): void {
    this.register = event === 'register' ? true : false;
  }
}

