import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { User } from '../usuario.model';

import { Authentication } from '../../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output() public displayPanel: EventEmitter<string> = new EventEmitter<string>();

  public form: FormGroup = new FormGroup({
    'email': new FormControl(null),
    'full_name': new FormControl(null),
    'user_name': new FormControl(null),
    'password': new FormControl(null)
  });

  constructor(
    private authentication: Authentication
  ) { }

  ngOnInit() {
  }

  public displayPanelLogin(): void {
    this.displayPanel.emit('login');
  }

  public registerUser(): void {

    const user: User = new User(
      this.form.value.email,
      this.form.value.full_name,
      this.form.value.user_name,
      this.form.value.password
    );

    this.authentication.registerUser(user)
      .then(() => this.displayPanelLogin());
  }

}
