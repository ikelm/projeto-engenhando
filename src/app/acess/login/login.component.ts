import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Authentication } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private detail = 'Entre com seu login e senha!';

  @Output() public displayPanel: EventEmitter<string> = new EventEmitter<string>();

  public form: FormGroup = new FormGroup({
    'email': new FormControl(null),
    'password': new FormControl(null)
  });

  constructor(
    private authentication: Authentication
  ) { }

  ngOnInit() {
  }

  public displayPanelRegister(): void {
    this.displayPanel.emit('register');
  }

  public authenticate(): void {
    this.authentication.authenticate(
      this.form.value.email,
      this.form.value.password
    );
  }

}
