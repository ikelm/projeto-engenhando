export class User {
    constructor(
        public email: string,
        public full_name: string,
        public user_name: string,
        public password: string
    ) {}
}
