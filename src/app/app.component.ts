import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  ngOnInit(): void {

    const config = {
      apiKey: 'AIzaSyDEIl0NT0KvpG58AZFuAQ854BQzbsy5ik0',
      authDomain: 'projeto-engenhando-504e1.firebaseapp.com',
      databaseURL: 'https://projeto-engenhando-504e1.firebaseio.com',
      projectId: 'projeto-engenhando-504e1',
      storageBucket: 'projeto-engenhando-504e1.appspot.com',
      messagingSenderId: '825528067132'
    };

    firebase.initializeApp(config);

  }
}
