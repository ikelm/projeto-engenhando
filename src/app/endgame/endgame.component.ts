import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-endgame',
  templateUrl: './endgame.component.html',
  styleUrls: ['./endgame.component.css']
})
export class EndgameComponent implements OnInit {

  public jogoEmAndamento = true;
  public tipoEncerramento: string;

  constructor() { }

  ngOnInit() {
  }

  public encerrarJogo(tipo: string): void {
    this.jogoEmAndamento = false;
    this.tipoEncerramento = tipo;
  }

  public reiniciarJogo(): void {
  this.jogoEmAndamento = true;
  this.tipoEncerramento = undefined;
  }


}
