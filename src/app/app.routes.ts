import { Routes } from '@angular/router';

import { AcessComponent } from './acess/access.component';
import { HomeComponent } from './home/home.component';

import { AuthenticationGuard } from './services/authentication-guard.service';

export const ROUTES: Routes = [
    { path: '', component: AcessComponent },
    { path: 'home', component: HomeComponent, canActivate: [ AuthenticationGuard ] },
];
