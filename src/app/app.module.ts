import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ROUTES } from './app.routes';

import { Authentication } from './services/authentication.service';
import { AuthenticationGuard } from './services/authentication-guard.service';
import { Progresso } from './services/progresso.service';

import { AppComponent } from './app.component';
import { AcessComponent } from './acess/access.component';
import { LoginComponent } from './acess/login/login.component';
import { RegisterComponent } from './acess/register/register.component';
import { HomeComponent } from './home/home.component';
import { AttemptsComponent } from './attempts/attempts.component';
import { ProgressComponent } from './progress/progress.component';
import { TopPanelComponent } from './top-panel/top-panel.component';
import { EndgameComponent } from './endgame/endgame.component';

@NgModule({
  declarations: [
    AppComponent,
    AcessComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AttemptsComponent,
    ProgressComponent,
    TopPanelComponent,
    EndgameComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [ Authentication, AuthenticationGuard, Progresso ],
  bootstrap: [AppComponent]
})
export class AppModule { }
